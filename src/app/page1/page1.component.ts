import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {EmployeeDetailService} from '../employeeDetail.service';
import {LoaderService} from '../_service/loader.service';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.css']
})
export class Page1Component implements OnInit {

  apiData;
  searchName;
  order;
  ascending = true;

  constructor(private employeeService: EmployeeDetailService,
              private route: Router,
              private loaderService: LoaderService) { }

  ngOnInit(): void {
    this.employeeService.getdata()
      .subscribe((data) => {
        this.apiData = data;
      });
  }

  searchByName() {
    const name = this.searchName;
    if (this.searchName) {
      this.apiData = this.apiData.filter(function(tag) {
        return tag.indexOf(name) >= 0;
      });
    } else {
      this.employeeService.getdata()
        .subscribe((data) => {
          this.apiData = data;
        });
    }
  }

  sortName() {
    this.order = 'name';
    if (this.ascending === true) {
      this.ascending = false;
    } else {
      this.ascending = true;
    }
  }
}
