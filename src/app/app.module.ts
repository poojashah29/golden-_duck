import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';

import {SortingPipe} from './sorting.pipe';

import { AppComponent } from './app.component';
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';

import {EmployeeDetailService} from './employeeDetail.service';
import {LoaderService} from './_service/loader.service';


@NgModule({
  declarations: [
    AppComponent,
    Page1Component,
    Page2Component,
    SortingPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgxPaginationModule,
  ],
  providers: [EmployeeDetailService, LoaderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
