import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable()
export class EmployeeDetailService {
  constructor(private http: HttpClient) {
  }

  getdata() {
    return this.http.get('http://cp-viewer.cloudhub.io/api/v1/view/all');
  }

  getApiData(name) {
    return this.http.get('http://cp-viewer.cloudhub.io/api/v1/view/' + name);
  }
}
