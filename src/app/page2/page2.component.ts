import {Component, OnInit} from '@angular/core';
import {EmployeeDetailService} from '../employeeDetail.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {LoaderService} from '../_service/loader.service';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.css'],
  providers: [DatePipe]

})
export class Page2Component implements OnInit {
  // searchUserName;
  startDate;
  endDate;
  showData = false;
  btcPredictData;
  priceVolumePolarityFeedData;
  order;
  error = false;
  ascending = true;

  constructor(private employeeDetailService: EmployeeDetailService,
              private router: Router,
              private route: ActivatedRoute,
              private datePipe: DatePipe,
              private loaderService: LoaderService) {
  }

  ngOnInit(): void {
    this.loaderService.display(true);
    const id = this.route.snapshot.params['name'];
    if (id === 'btc-predict') {
      this.employeeDetailService.getApiData(id)
        .subscribe((data) => {
          if (!data) {
            this.error = true;
            this.loaderService.display(false);
          } else {
            this.btcPredictData = data;
            this.loaderService.display(false);
          }
        });
      this.showData = true;
    } else {
      this.employeeDetailService.getApiData(id)
        .subscribe((data) => {
          if (!data) {
            this.error = true;
            this.loaderService.display(false);
          } else {
            this.priceVolumePolarityFeedData = data;
            this.loaderService.display(false);
          }
        });
    }
  }

  sort(element) {
    if (element === 'timestamp') {
      this.order = 'timestamp';
    } else if (element === 'Time') {
      this.order = 'Time';
    } else if (element === 'currentPrice') {
      this.order = 'currentPrice';
    } else {
      this.order = 'Price';
    }
    if (this.ascending === true) {
      this.ascending = false;
    } else {
      this.ascending = true;
    }
  }
  searchByTimestamp() {
    const id = this.route.snapshot.params['name'];
    if (this.startDate !== undefined && this.endDate !== undefined) {
      if (this.startDate !== '' && this.endDate !== '') {
        if (this.startDate < this.endDate) {
          if (id === 'btc-predict') {
            const sDate = this.datePipe.transform(this.startDate, 'yy-MM-dd');
            const eDate = this.datePipe.transform(this.endDate, 'yy-MM-dd');
            this.btcPredictData = this.btcPredictData.filter(function (tag) {
              return tag.timestamp >= sDate && tag.timestamp < eDate;
            });
            } else {
            const sDate = this.datePipe.transform(this.startDate, 'yyyy-MM-dd');
            const eDate = this.datePipe.transform(this.endDate, 'yyyy-MM-dd');
            this.priceVolumePolarityFeedData = this.priceVolumePolarityFeedData.filter(function (data) {
              return data.Time >= sDate && data.Time < eDate;
            });
          }
        } else {
          alert('End Date Must Be Greater Than Start Date');
        }
      } else {
        if (id === 'btc-predict') {
          this.employeeDetailService.getApiData('btc-predict')
            .subscribe((data) => {
              this.btcPredictData = data;
            });
        } else {
          this.employeeDetailService.getApiData('price_volume_polarity_feed')
            .subscribe((data) => {
              this.priceVolumePolarityFeedData = data;
            });

        }
              }
    } else {
       alert('select startDate and End Date');
    }
  }
  clearFilter() {
    this.loaderService.display(true);
    this.startDate = '';
    this.endDate = '';
    const id = this.route.snapshot.params['name'];
    if (id === 'btc-predict') {
      this.employeeDetailService.getApiData('btc-predict')
        .subscribe((data) => {
          this.btcPredictData = data;
        });
      this.loaderService.display(false);
    } else {
      this.employeeDetailService.getApiData('price_volume_polarity_feed')
        .subscribe((data) => {
          this.priceVolumePolarityFeedData = data;
        });
      this.loaderService.display(false);
    }
  }
}
